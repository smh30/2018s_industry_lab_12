package ictgradschool.industry.bounce;

import java.awt.*;

public class DynamicRectangleShape extends RectangleShape {

    private Color c;
    private Boolean filled = false;

    public DynamicRectangleShape() {
        super();
    }

    public DynamicRectangleShape(Color a) {
        super();
        c = a;
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, Color a) {
        super(x, y, deltaX, deltaY);
        c = a;
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Color a) {
        super(x, y, deltaX, deltaY, width, height);
        c=a;
    }

    public void move(int width, int height){
        super.move(width, height);
        if (fX+fWidth == width || fX== 0){
            filled = true;
        } else if (fY == 0 || fY+fHeight == height){
            filled = false;
        }
    }

    @Override
    public void paint (Painter painter){
        if(c != null){
            painter.setColor(c);
        }
        if (filled){
            painter.fillRect(fX, fY, fWidth, fHeight);
        } else {
            painter.drawRect(fX, fY, fWidth, fHeight);
        }
        painter.setColor(Color.black);
    }


}


