package ictgradschool.industry.bounce;

import java.awt.*;

public class GemShape extends Shape {
    int[] xpts;
    int[] ypts;
    int npts;

    // create an default gemBox
    public GemShape() {
        super();
        createGem();
           }

    //creat an gemBox with some params
    public GemShape (int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
        createGem();
            }

    //create an gemBox with many params
    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
        createGem();
    }

    // create the actual gem shape within the given box
    public void createGem(){
        if (fWidth > 40){
            xpts = new int[]{fX+20, fX+fWidth-20, fX+fWidth, fX+fWidth-20, fX+20, fX};
            ypts = new int[]{fY,fY,fY+fHeight/2,fY+fHeight, fY+fHeight, fY+fHeight/2};
            npts = 6;
        } else {
            xpts = new int[]{fX+fWidth/2, fX+fWidth, fX+fWidth/2, fX};
            ypts = new int[]{fY, fY+fHeight/2, fY+fHeight, fY+fHeight/2};
            npts = 4;
        }
    }



    @Override
    public void paint(Painter painter) {
        createGem();
        painter.drawPolygon(new Polygon(xpts, ypts, npts));
    }
}
