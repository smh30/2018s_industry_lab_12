package ictgradschool.industry.bounce;

// to make an oval is exactly the same as a rectangle, but when drawing it
// make an oval rather than a rectangle
public class OvalShape extends Shape {

    // create an default oval
    public OvalShape() {super();}

    //creat an oval with some params
    public OvalShape (int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
    }

    //create an oval with many params
    public OvalShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }

    @Override
    public void paint(Painter painter) {
painter.drawOval(fX, fY, fWidth, fHeight);
    }

}
