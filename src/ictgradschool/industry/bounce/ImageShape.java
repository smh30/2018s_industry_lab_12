package ictgradschool.industry.bounce;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageShape extends Shape {

    BufferedImage image = null;

    public ImageShape() {
        super();

        try{
            image = ImageIO.read(getClass().getResource("starry.png"));
        } catch(Exception e){e.printStackTrace();}

        if (image != null) {
            fWidth = image.getWidth();
            fHeight = image.getHeight();
        }
    }


    public ImageShape(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);

        try{
            image = ImageIO.read(getClass().getResource("starry.png"));
        } catch(Exception e){e.printStackTrace();}

        if (image !=null) {
            fWidth = image.getWidth();
            fHeight = image.getHeight();
        }
    }



    @Override
    public void paint(Painter painter) {
        //painter.drawRect(fX,fY,fWidth,fHeight);
        painter.drawImg(image, fX, fY);
    }

}
